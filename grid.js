"uses strict";


//------------creation d'une ligne de la grille

class Grid{
  constructor(){
    this.tiles = [];

  }
  generate(){
    this.tiles = [];
    for (let index = 1; index <= 8; index++) {
      let tileA = new Tile(`image-${index}`);
      let tileB = new Tile(`image-${index}`);
      this.tiles.push(tileA, tileB);
    }
  }
}